package com.example.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the detallefactura database table.
 * 
 */
@Entity
@NamedQuery(name="Detallefactura.findAll", query="SELECT d FROM Detallefactura d")
public class Detallefactura implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private DetallefacturaPK id;

	private int cantidad;

	@Column(name="porcentaje_ganancia")
	private double porcentajeGanancia;

	//bi-directional many-to-one association to Articulo
	@ManyToOne
	@JoinColumn(name="codigo_articulo")
	private Articulo articulo;

	//bi-directional many-to-one association to Factura
	@ManyToOne
	@JoinColumn(name="numero_factura", referencedColumnName="numero_factura",  insertable=false, updatable=false)
	private Factura factura;

	public Detallefactura() {
	}

	public DetallefacturaPK getId() {
		return this.id;
	}

	public void setId(DetallefacturaPK id) {
		this.id = id;
	}

	public int getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public double getPorcentajeGanancia() {
		return this.porcentajeGanancia;
	}

	public void setPorcentajeGanancia(double porcentajeGanancia) {
		this.porcentajeGanancia = porcentajeGanancia;
	}

	public Articulo getArticulo() {
		return this.articulo;
	}

	public void setArticulo(Articulo articulo) {
		this.articulo = articulo;
	}

	public Factura getFactura() {
		return this.factura;
	}

	public void setFactura(Factura factura) {
		this.factura = factura;
	}

}