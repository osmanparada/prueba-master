package com.example.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the detallefactura database table.
 * 
 */
@Embeddable
public class DetallefacturaPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="numero_factura", insertable=false, updatable=false)
	private String numeroFactura;

	@Column(name="numero_detalle_factura")
	private String numeroDetalleFactura;

	public DetallefacturaPK() {
	}
	public String getNumeroFactura() {
		return this.numeroFactura;
	}
	public void setNumeroFactura(String numeroFactura) {
		this.numeroFactura = numeroFactura;
	}
	public String getNumeroDetalleFactura() {
		return this.numeroDetalleFactura;
	}
	public void setNumeroDetalleFactura(String numeroDetalleFactura) {
		this.numeroDetalleFactura = numeroDetalleFactura;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof DetallefacturaPK)) {
			return false;
		}
		DetallefacturaPK castOther = (DetallefacturaPK)other;
		return 
			this.numeroFactura.equals(castOther.numeroFactura)
			&& this.numeroDetalleFactura.equals(castOther.numeroDetalleFactura);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.numeroFactura.hashCode();
		hash = hash * prime + this.numeroDetalleFactura.hashCode();
		
		return hash;
	}
}