package com.example.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the proveedor database table.
 * 
 */
@Entity
@NamedQuery(name="Proveedor.findAll", query="SELECT p FROM Proveedor p")
public class Proveedor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="cif_proveedor")
	private String cifProveedor;

	private String apelido1;

	private String apelido2;

	private String ciudad;

	private String cp;

	private String direccion;

	private String nombre;

	@Column(name="nombre_empresa")
	private String nombreEmpresa;

	private String pais;

	private String provincia;

	//bi-directional many-to-one association to Articulo
	@OneToMany(mappedBy="proveedor", fetch=FetchType.EAGER)
	private List<Articulo> articulos;

	public Proveedor() {
	}

	public String getCifProveedor() {
		return this.cifProveedor;
	}

	public void setCifProveedor(String cifProveedor) {
		this.cifProveedor = cifProveedor;
	}

	public String getApelido1() {
		return this.apelido1;
	}

	public void setApelido1(String apelido1) {
		this.apelido1 = apelido1;
	}

	public String getApelido2() {
		return this.apelido2;
	}

	public void setApelido2(String apelido2) {
		this.apelido2 = apelido2;
	}

	public String getCiudad() {
		return this.ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getCp() {
		return this.cp;
	}

	public void setCp(String cp) {
		this.cp = cp;
	}

	public String getDireccion() {
		return this.direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNombreEmpresa() {
		return this.nombreEmpresa;
	}

	public void setNombreEmpresa(String nombreEmpresa) {
		this.nombreEmpresa = nombreEmpresa;
	}

	public String getPais() {
		return this.pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getProvincia() {
		return this.provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public List<Articulo> getArticulos() {
		return this.articulos;
	}

	public void setArticulos(List<Articulo> articulos) {
		this.articulos = articulos;
	}

	public Articulo addArticulo(Articulo articulo) {
		getArticulos().add(articulo);
		articulo.setProveedor(this);

		return articulo;
	}

	public Articulo removeArticulo(Articulo articulo) {
		getArticulos().remove(articulo);
		articulo.setProveedor(null);

		return articulo;
	}

}