package com.example.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.data.Reporte;
import com.example.model.Articulo;

public interface ArticuloRepository extends JpaRepository<Articulo, String> {
	

	@Query("select new com.example.repository.ArticuloSalida(a.codigoArticulo, a.caracteristicas,a.nombreArticulo) from Articulo a ")
	public List<ArticuloSalida>  listarArticulos();
	
	
	@Query(value ="SELECT " + 
			" new com.example.data.Reporte(a.codigoArticulo ,a.nombreArticulo,f.fechaFactura,(SELECT SUM(df.cantidad) FROM Detallefactura df where df.factura.numeroFactura= f.numeroFactura) ) " + 
			" FROM Factura f" + 
			" JOIN Detallefactura df on  df.id.numeroFactura= f.numeroFactura" + 
			" JOIN Articulo a on a.codigoArticulo= df.articulo.codigoArticulo" + 
			" GROUP BY df.articulo.codigoArticulo, f.fechaFactura" + 
			" ORDER BY f.fechaFactura, cantidad DESC", nativeQuery = false)
	public List<Reporte>  reporte();

}
