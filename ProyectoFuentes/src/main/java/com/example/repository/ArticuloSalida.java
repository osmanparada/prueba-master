package com.example.repository;

public class ArticuloSalida {


		public String codigoArticulo;
		public String caracteristicas;
		public String nombreArticulo;
		
		public ArticuloSalida(String codigoArticulo, String caracteristicas, String nombreArticulo) {
			super();
			this.codigoArticulo = codigoArticulo;
			this.caracteristicas = caracteristicas;
			this.nombreArticulo = nombreArticulo;
		}
		
		public String getCodigoArticulo() {
			return codigoArticulo;
		}
		public void setCodigoArticulo(String codigoArticulo) {
			this.codigoArticulo = codigoArticulo;
		}
		public String getCaracteristicas() {
			return caracteristicas;
		}
		public void setCaracteristicas(String caracteristicas) {
			this.caracteristicas = caracteristicas;
		}
		public String getNombreArticulo() {
			return nombreArticulo;
		}
		public void setNombreArticulo(String nombreArticulo) {
			this.nombreArticulo = nombreArticulo;
		}
}
