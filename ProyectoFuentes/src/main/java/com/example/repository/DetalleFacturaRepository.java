package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;


import com.example.model.Detallefactura;
import com.example.model.DetallefacturaPK;

public interface DetalleFacturaRepository  extends JpaRepository<Detallefactura, DetallefacturaPK>  {

}
