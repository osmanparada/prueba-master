package com.example.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.model.Cliente;


@Repository("clienteRepository")
public interface ClienteRepository  extends JpaRepository<Cliente, String> {
	
	//SELECT CASE WHEN COUNT(f) >0 THEN 'true' ELSE 'false' END FROM Factura f WHERE f.id=?1
	@Transactional
	@Modifying
	@Query("delete from Cliente c where c.cifCliente= ?1")
	public void eliminarCLiente(String cif);
	
	@Query("select c from Cliente c where c.cifCliente like %?1% ")
	public List<Cliente>  buscarCLientes(String cif);
	
	@Query("select c from Cliente c where c.cifCliente = ?1 ")
	public Cliente  buscarCLiente(String cifCliente);
	
	@Query("select new com.example.data.ClienteData(c.cifCliente,c.nombre,c.apelido1,c.apelido2,c.direccion) from Cliente c ")
	public List<com.example.data.ClienteData>  buscarTodos();


	


}
