package com.example.data;

import java.io.Serializable;
import java.util.Date;



public class Reporte implements Serializable {



	private String codigo_articulo;
	
	
	

	private String nombre_articulo;
	
	
	private Date fecha_factura;
	

	private long cantidad;
	
	public Reporte(String codigo_articulo, String nombre_articulo, Date fecha_factura, long cantidad) {
		super();
		this.codigo_articulo = codigo_articulo;
		this.nombre_articulo = nombre_articulo;
		this.fecha_factura = fecha_factura;
		this.cantidad = cantidad;
	}



	public String getCodigo_articulo() {
		return codigo_articulo;
	}


	public void setCodigo_articulo(String codigo_articulo) {
		this.codigo_articulo = codigo_articulo;
	}


	public String getNombre_articulo() {
		return nombre_articulo;
	}


	public void setNombre_articulo(String nombre_articulo) {
		this.nombre_articulo = nombre_articulo;
	}


	public Date getFecha_factura() {
		return fecha_factura;
	}


	public void setFecha_factura(Date fecha_factura) {
		this.fecha_factura = fecha_factura;
	}


	public long getCantidad() {
		return cantidad;
	}


	public void setCantidad(long cantidad) {
		this.cantidad = cantidad;
	}
	
	
}
