package com.example.data;


public class ClienteData {
	
	private String cifCliente;

	

	private String apelido1;

	private String apelido2;

	private String ciudad;

	private String cp;

	private String direccion;

	private String nombre;
	private String pais;

	private String provincia;
	
	
	public ClienteData(String cifCliente,String nombre, String apelido1, String apelido2,
			String direccion) {
		this.cifCliente = cifCliente;
		this.apelido1 = apelido1;
		this.apelido2 = apelido2;
		this.direccion = direccion;
		this.nombre = nombre;
	}
	


	public String getCifCliente() {
		return cifCliente;
	}

	public void setCifCliente(String cifCliente) {
		this.cifCliente = cifCliente;
	}

	public String getApelido1() {
		return apelido1;
	}

	public void setApelido1(String apelido1) {
		this.apelido1 = apelido1;
	}

	public String getApelido2() {
		return apelido2;
	}

	public void setApelido2(String apelido2) {
		this.apelido2 = apelido2;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getCp() {
		return cp;
	}

	public void setCp(String cp) {
		this.cp = cp;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}


}
