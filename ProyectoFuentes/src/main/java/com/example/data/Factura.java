package com.example.data;

import java.util.List;

public class Factura {
	
	
	public String numeroFactura;
	public String cifCliente;
	public List<ProductoComprado> listaProductoComprado;
	public String total;
	
	public String getCifCliente() {
		return cifCliente;
	}
	public void setCifCliente(String cifCliente) {
		this.cifCliente = cifCliente;
	}
	public List<ProductoComprado> getListaProductoComprado() {
		return listaProductoComprado;
	}
	public void setListaProductoComprado(List<ProductoComprado> listaProductoComprado) {
		this.listaProductoComprado = listaProductoComprado;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public String getNumeroFactura() {
		return numeroFactura;
	}
	public void setNumeroFactura(String numeroFactura) {
		this.numeroFactura = numeroFactura;
	}

}
