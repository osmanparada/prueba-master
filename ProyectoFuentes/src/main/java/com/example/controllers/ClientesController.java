package com.example.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.data.ProductoComprado;
import com.example.data.Reporte;
import com.example.model.Articulo;
import com.example.model.Cliente;
import com.example.model.Detallefactura;
import com.example.model.DetallefacturaPK;
import com.example.model.Factura;
import com.example.repository.ArticuloRepository;
import com.example.repository.ArticuloSalida;
import com.example.repository.ClienteRepository;
import com.example.repository.DetalleFacturaRepository;
import com.example.repository.FacturaRepository;
import com.example.repository.Queries;


@RestController
public class ClientesController {
	
	@Autowired
	private ClienteRepository clienteRepository;
	 
	@Autowired
	private ArticuloRepository articuloRepository;
	
	@Autowired
	private FacturaRepository facturaRepository;
	
	@Autowired
	private DetalleFacturaRepository detalleFacturaRepository;
	

	 
	
	@CrossOrigin(origins = "*")
	@GetMapping("/allClients")	 
	public List<com.example.data.ClienteData> getAllClients() {
		
		return clienteRepository.buscarTodos();
	}
	
	@PostMapping("saveClient")
	public Response saveClient(@RequestBody Cliente cliente) {
		Response resp= new Response();
		try {
			clienteRepository.save(cliente);
			resp.setRes( "OK");
		}catch(Exception ex) {
			ex.printStackTrace();
			resp.setRes( "ER");
		}
		return resp;
	}
	

	@PostMapping("deleteClient")
	public Response deleteClient(@RequestBody String cif) {
		System.out.println("cif: "+cif);
		
		Response resp= new Response();
		try { 
			clienteRepository.eliminarCLiente(cif);
			resp.setRes("OK");
		}catch(Exception ex) {
			ex.printStackTrace();
			resp.setRes("ER");
		}
		return resp;
		
	}
	
	@PostMapping("searchClients")
	public List<Cliente> searchClients(@RequestBody String cif){
		return clienteRepository.buscarCLientes(cif);
	}
	
	@GetMapping("/getAllProducts")	 
	public List<ArticuloSalida> getAllProducts() {
		
		return articuloRepository.listarArticulos();
	}
	
	@PostMapping("guardarFactura")	 
	public Response guardarFactura(@RequestBody com.example.data.Factura factura) {
		Response resp= new Response();
		System.out.println("***************************************************************************");
		System.out.println("Factura: "+factura.getNumeroFactura());
		Factura fact= new Factura();
		Cliente cliente = clienteRepository.buscarCLiente(factura.getCifCliente());
		System.out.println(cliente.getNombre());
		fact.setCliente(cliente );
		fact.setFechaFactura(new Date());
		fact.setNumeroFactura(factura.getNumeroFactura());
		facturaRepository.save(fact);
		
		List<Detallefactura> detallefacturas = new ArrayList<>();
		for(ProductoComprado produ:factura.getListaProductoComprado()) {
			 int min = 50;
		     long max = 1000000000;
		     
		     long random_int = (int)(Math.random() * (max - min + 1) + min);
		     System.out.println("id: "+random_int);
			
			Detallefactura det= new Detallefactura();
			Articulo articulo= articuloRepository.findById(produ.codigoArticulo).get();
			System.out.println("acaaaaaaaaa: "+articulo.getNombreArticulo());
			det.setArticulo(articulo);
			double porcentajeGanancia= ((Double.parseDouble(produ.precioCompra) / Double.parseDouble(produ.precioVenta) )*100);
			double por= 100-porcentajeGanancia;
			det.setPorcentajeGanancia(por);
			det.setCantidad(Integer.parseInt(produ.cantidad));
			DetallefacturaPK id = new DetallefacturaPK();
			id.setNumeroDetalleFactura(String.valueOf(random_int));
			id.setNumeroFactura(factura.numeroFactura);
			det.setId(id );
			//detallefacturas.add(det);
			detalleFacturaRepository.save(det);
		}
		 
		//fact.setDetallefacturas(detallefacturas);
		
		
		return resp;
	}
	
	@PostMapping("reporteArticulos")
	public List<Reporte> reporteArticulos(@RequestBody String mes) {
		System.out.println("mes: "+mes);
		return articuloRepository.reporte();
	}

}
