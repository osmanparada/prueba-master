CREATE DATABASE  IF NOT EXISTS `mercados_chapinero` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `mercados_chapinero`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: mercados_chapinero
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.21-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `articulo`
--

DROP TABLE IF EXISTS `articulo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `articulo` (
  `codigo_articulo` varchar(20) NOT NULL,
  `cif_proveedor` varchar(30) NOT NULL,
  `nombre_articulo` varchar(40) NOT NULL,
  `caracteristicas` varchar(100) NOT NULL,
  PRIMARY KEY (`codigo_articulo`),
  KEY `cif_proveedor` (`cif_proveedor`),
  CONSTRAINT `articulo_ibfk_1` FOREIGN KEY (`cif_proveedor`) REFERENCES `proveedor` (`cif_proveedor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `articulo`
--

LOCK TABLES `articulo` WRITE;
/*!40000 ALTER TABLE `articulo` DISABLE KEYS */;
INSERT INTO `articulo` VALUES ('0001','1234567','Pasta de dientes','Pasta de dientes'),('0002','1234567','Arroz','Arroz');
/*!40000 ALTER TABLE `articulo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cliente`
--

DROP TABLE IF EXISTS `cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cliente` (
  `cif_cliente` varchar(30) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `apelido1` varchar(30) NOT NULL,
  `apelido2` varchar(30) NOT NULL,
  `nombre_empresa` varchar(100) NOT NULL,
  `direccion` varchar(50) NOT NULL,
  `ciudad` varchar(30) NOT NULL,
  `cp` varchar(30) NOT NULL,
  `provincia` varchar(50) NOT NULL,
  `pais` varchar(50) NOT NULL,
  PRIMARY KEY (`cif_cliente`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cliente`
--

LOCK TABLES `cliente` WRITE;
/*!40000 ALTER TABLE `cliente` DISABLE KEYS */;
INSERT INTO `cliente` VALUES ('1023584447','Luis','Morales','Ortiz','','cra 4','','','',''),('1023875866','Jose','Pepito','Pepito','','cra 23 34 45','','','','');
/*!40000 ALTER TABLE `cliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detallefactura`
--

DROP TABLE IF EXISTS `detallefactura`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detallefactura` (
  `numero_factura` varchar(20) NOT NULL,
  `numero_detalle_factura` varchar(20) NOT NULL,
  `codigo_articulo` varchar(20) NOT NULL,
  `cantidad` int(12) NOT NULL,
  `porcentaje_ganancia` double NOT NULL,
  PRIMARY KEY (`numero_factura`,`numero_detalle_factura`),
  KEY `codigo_articulo` (`codigo_articulo`),
  CONSTRAINT `detallefactura_ibfk_1` FOREIGN KEY (`numero_factura`) REFERENCES `factura` (`numero_factura`),
  CONSTRAINT `detallefactura_ibfk_2` FOREIGN KEY (`codigo_articulo`) REFERENCES `articulo` (`codigo_articulo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detallefactura`
--

LOCK TABLES `detallefactura` WRITE;
/*!40000 ALTER TABLE `detallefactura` DISABLE KEYS */;
INSERT INTO `detallefactura` VALUES ('1003','170469301','0001',3,33.33333333333334),('1003','27860669','0001',3,33.33333333333334),('1004','278549493','0002',30,40),('1005','564545454','0001',3,20),('1006','54756454','0001',4,10);
/*!40000 ALTER TABLE `detallefactura` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `factura`
--

DROP TABLE IF EXISTS `factura`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `factura` (
  `numero_factura` varchar(20) NOT NULL,
  `fecha_factura` date NOT NULL,
  `cif_cliente` varchar(30) NOT NULL,
  PRIMARY KEY (`numero_factura`),
  KEY `cif_cliente` (`cif_cliente`),
  CONSTRAINT `factura_ibfk_1` FOREIGN KEY (`cif_cliente`) REFERENCES `cliente` (`cif_cliente`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `factura`
--

LOCK TABLES `factura` WRITE;
/*!40000 ALTER TABLE `factura` DISABLE KEYS */;
INSERT INTO `factura` VALUES ('1003','2021-02-01','1023875866'),('1004','2021-02-01','1023875866'),('1005','2021-01-01','1023875866'),('1006','2021-01-02','1023875866');
/*!40000 ALTER TABLE `factura` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proveedor`
--

DROP TABLE IF EXISTS `proveedor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proveedor` (
  `cif_proveedor` varchar(30) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `apelido1` varchar(30) NOT NULL,
  `apelido2` varchar(30) NOT NULL,
  `nombre_empresa` varchar(100) NOT NULL,
  `direccion` varchar(50) NOT NULL,
  `ciudad` varchar(30) NOT NULL,
  `cp` varchar(30) NOT NULL,
  `provincia` varchar(50) NOT NULL,
  `pais` varchar(50) NOT NULL,
  PRIMARY KEY (`cif_proveedor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proveedor`
--

LOCK TABLES `proveedor` WRITE;
/*!40000 ALTER TABLE `proveedor` DISABLE KEYS */;
INSERT INTO `proveedor` VALUES ('1234567','Pepito','Perez','','Bodega','cra 1','Bogotá','','Bogotá','Colombia');
/*!40000 ALTER TABLE `proveedor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'mercados_chapinero'
--

--
-- Dumping routines for database 'mercados_chapinero'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-02-02  8:17:29
