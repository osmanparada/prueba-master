import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { FormClientComponent } from './form-client/form-client.component';
import { Routes, RouterModule } from "@angular/router";

import { FormsModule } from '@angular/forms'  
import { ReactiveFormsModule} from '@angular/forms' 

const routes: Routes = [
  { path: '', redirectTo: '/AppComponent', pathMatch: 'full' },
  { path: 'formClient', component: FormClientComponent },

];


@NgModule({
  declarations: [
    AppComponent,
    FormClientComponent,
  ],
  imports: [
    RouterModule.forRoot(routes) ,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NgbModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule 
    
  ],
  exports: [ RouterModule ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
