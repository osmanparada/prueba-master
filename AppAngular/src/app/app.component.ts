import { Component } from '@angular/core';
import { ApiService } from "src/app/ClienService/api.service";
import { Cliente } from "src/app/models/cliente";
import { RouterModule, Router } from '@angular/router';

import { NgbModal, ModalDismissReasons, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import {
  ValidatorFn,
  Validator,
  AbstractControl,
  FormControl,
  NG_VALIDATORS,
  FormBuilder,
  Validators,
} from '@angular/forms';
import { Articulo } from './models/articulo';
import { ProductoComprado } from './models/producto_comprado';
import { Factura } from './models/factura';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'lucasian';

  loading: boolean = false;
  errorMessage;
  clientes: Cliente[];

  mostrarRadio: boolean = false;

  clienteRadio: string;

  responseEliminar: string = "";

  modalOptions: NgbModalOptions;
  closeResult: string;
  selecc: string = null;
  busqueda: string = "";

  articulos: Articulo[];

  cantidad:any=0;

  art:Articulo;

  productosComprados: ProductoComprado[]= [];

  precioCompra:any=0;
  precioVenta:any=0;
  total:any=0;
  numeroFactura:string="";
  cifCliente:string="";


  checkoutForm = this.formBuilder.group({
    cifCliente: ['', Validators.required],
    apelido1: ['', Validators.required],
    apelido2: ['', Validators.required],
    direccion: ['', Validators.required],
    nombre: ['', Validators.required],
  });


  constructor(private apiService: ApiService, private router: Router, private modalService: NgbModal,
    private formBuilder: FormBuilder) {
    this.modalOptions = {
      backdrop: 'static',
      backdropClass: 'customBackdrop'
    }

    this.buscarArticulos();
  }


  public getCliets() {
    this.mostrarRadio = false;
    this.loading = true;
    this.errorMessage = "";
    this.apiService.getAllClients()
      .subscribe(
        (response) => {                           //next() callback
          console.log('response received')
          this.clientes = response;
        },
        (error) => {                              //error() callback
          console.error('Request failed with error')
          this.errorMessage = error;
          this.loading = false;
        },
        () => {                                   //complete() callback
          console.error('Request completed')      //This is actually not needed 
          this.loading = false;
        })
  }

  cambiarMostrarRadio(cif) {
    this.cifCliente=cif;
    this.mostrarRadio = true;

  }

  cambiarradioValue(cliente) {
    console.log("id: " + cliente.cifCliente);
    this.cifCliente=cliente.cifCliente;
    this.clienteRadio = cliente.cifCliente;
    this.checkoutForm.controls['cifCliente'].setValue(cliente.cifCliente);
    this.checkoutForm.controls['apelido1'].setValue(cliente.apelido1);
    this.checkoutForm.controls['apelido2'].setValue(cliente.apelido2);
    this.checkoutForm.controls['direccion'].setValue(cliente.direccion);
    this.checkoutForm.controls['nombre'].setValue(cliente.nombre);

  }

  eliminarCliente() {
    this.apiService.eliminarCliente(this.clienteRadio).subscribe(
      //(response)=> this.responseEliminar=response,
      (response) => { this.getCliets() },
      (error) => console.log(error)
    );
    console.log("Respuesta de eliminar: " + this.responseEliminar);
  }

  navFormClient() {
    this.router.navigate(['/formClient']);
  }

  open(content, val: boolean) {
    this.total=0;
    this.productosComprados=[];
    this.numeroFactura="";

    console.log(val);
    if (val) {
      this.checkoutForm.reset();
      this.selecc = null;
    }
    this.modalService.open(content, this.modalOptions).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  guardarCliente() {

    console.log(this.checkoutForm.get('nombre').value);

    let cliente: Cliente = {

      cifCliente: this.checkoutForm.get('cifCliente').value,
      apelido1: this.checkoutForm.get('apelido1').value,
      apelido2: this.checkoutForm.get('apelido2').value,
      ciudad: '',
      cp: '',
      direccion: this.checkoutForm.get('direccion').value,
      nombre: this.checkoutForm.get('nombre').value,
      nombreEmpresa: '',
      pais: '',
      provincia: '',
    }

    this.apiService.guardarCliente(cliente).subscribe(
      (response) => {
        console.log("recargar clientes");
        this.getCliets();
      },
      (error) => console.log(error)
    );

  }

  busquedaCliente(value) {
    this.busqueda = value;
  }

  buscar() {
    if (null != this.busqueda) {
      this.apiService.getClients(this.busqueda).subscribe(
        (response) => {                           //next() callback
          console.log('response received')
          this.clientes = response;
        },
        (error) => {                              //error() callback
          console.error('Request failed with error')
          this.errorMessage = error;
          this.loading = false;
        },
      );
    } else {
      this.getCliets();
    }
  }

  buscarArticulos(){
    this.apiService.getAllProductos().subscribe(
      (response)=>{
        this.articulos=response;
      },
      (error)=>{console.log(error);}
    );
  }

  adicionarProductoFactura(){

    let produ : ProductoComprado={
        cantidad:this.cantidad,
        nombre:this.art.nombreArticulo,
        precioVenta: this.precioVenta,
        precioCompra:this.precioCompra,
        codigoArticulo:this.art.codigoArticulo,
        valorUnit:this.precioVenta,
        valorTotal:(this.precioVenta * this.cantidad),
    }
    console.log("total: "+produ.valorTotal);
    this.productosComprados.push(produ);
    this.precioVenta=0;
    this.cantidad=0;
    this.precioCompra=0;
    this.art=null;
    this.total=produ.valorTotal+this.total;
  }

  selectArt(cod){    
    for(let a of this.articulos){
      if(a.codigoArticulo== cod){
        this.art=a;
      }
    }
  }

  guardarfactura(){
    let factura:Factura={
      cifCliente:this.cifCliente,
      numeroFactura:this.numeroFactura,
      total: this.total,
      listaProductoComprado: this.productosComprados

    }
    this.apiService.saveFactura(factura).subscribe(
      (response)=>{alert('Factura guardada')},
      (error)=>{
        console.log(error);
        alert("No se pudo guardar la factura");
      }
    )
  }


}
