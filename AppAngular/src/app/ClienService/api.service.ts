import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Cliente } from '../models/cliente';
import { Observable, throwError } from "rxjs";
import { retry, catchError } from 'rxjs/operators';
import { tap } from "rxjs/internal/operators/tap";
import { Articulo } from '../models/articulo';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
    log: any;

   constructor(private http: HttpClient) { }


  getAllClients(): Observable<any> {
    console.log("Estoy acá");
    return this.http.get<Cliente>("http://localhost:8080/allClients");
   
  }

  eliminarCliente(cif){
    console.log("Eliminar cliente: "+cif);
    return this.http.post("http://localhost:8080/deleteClient",cif);
  }

  guardarCliente(cliente:Cliente) :Observable<any> {
    return this.http.post("http://localhost:8080/saveClient",cliente);
  }

  getClients(value): Observable<any> {
    return this.http.post<Cliente>("http://localhost:8080/searchClients", value);
   
  }

  getAllProductos(): Observable<any>{
    return this.http.get<Articulo>("http://localhost:8080/getAllProducts");
  }

  saveFactura(factura):Observable<any>{
    return this.http.post("http://localhost:8080/guardarFactura",factura);
  }

    
}



