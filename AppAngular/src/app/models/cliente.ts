

export interface Cliente{

     cifCliente: string;
     apelido1: string;
     apelido2: string;
     ciudad: string;
     cp: string;
     direccion: string;
     nombre: string;
     nombreEmpresa: string;
     pais: string;
     provincia: string;


}