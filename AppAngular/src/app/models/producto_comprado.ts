export interface ProductoComprado{
    codigoArticulo:string;
    cantidad:any;
    nombre:string;
    precioVenta:any;
    precioCompra:any;
    valorUnit:any;
    valorTotal:any;

}