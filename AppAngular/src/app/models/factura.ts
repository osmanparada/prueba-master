import { ProductoComprado } from "./producto_comprado";

export interface Factura{
    
    cifCliente:string;
    numeroFactura:string;

    listaProductoComprado:ProductoComprado[];

    total:any;

}