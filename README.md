# Prueba-Master



El Proyecto se realizo utilizando Spring-boot y Angular Cli. Para la base de datos se utilizo el gestor MySQL

Acontinuación el procedimiento de intalación de dependencias del proyecto.

1). Correr el dump.sql en gestor de Base de Datos MySQL.
2)Instalación dependencias y ejecución del proyecto fuente.

mvn install  // Para instalar dependencias
mvn spring-boot:run  // Para arrancar el proyecto

3). Instalación dependencias y ejecución de la aplicación Angular.

npm install // Para instalar las dependencias
ng serve --open // Para arrancar el proyecto y ver desde le navegador
Apuntar al index que se encuentra en src/index.html

4) Realizar las pruebas
